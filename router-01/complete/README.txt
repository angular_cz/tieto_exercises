Router-01

  Pro tato cvičení jsou připraveny testy. Spustíte je příkazem npm run router-01-protractor
  Protože jde o protractor testy, jejich spuštění trvá pár sekund a TDD vývoj oproti nim by nebyl příliš efektivní.
  Testy tedy použijte pro ověření správnosti jednotlivých úkolů.

1. definujte routu pro OrderListController

    pokud je url '/orders' použije se:
       šablona 'orderList.html', controller OrderListController as list      

    ... zdrojový kód TODO 1 definujte routu pro OrderListController -->
                                                                                                                    $routeProvider
                                                                                                                        .when('/orders', {
                                                                                                                          templateUrl: 'orderList.html',
                                                                                                                          controller: 'OrderListController',
                                                                                                                          controllerAs: 'list'
                                                                                                                        })

2. zajistěte přesměrování na seznam, i při zadání jiné adresy

    tzn. pokud adresa neodpovídá žádné z definovaných rout, přesměruje se na '/orders'

    ... zdrojový kód TODO 2 zajistěte přesměrování na seznam -->
                                                                                                                    $routeProvider
                                                                                                                        ...
                                                                                                                        .otherwise('/orders');

3. definujte routu pro OrderDetailController

      pokud je url '/detail/ID' (ID je číslo objednávky) použije se:
         šablona 'orderDetail.html', controller OrderDetailController as detail  
         a v resolve načtěte data z REST_URI + /orders/ID jako orderData  
          (nezapoměňte, že $http vám v then vrátí responce, ale do controlleru 
            potřebujete předat jen obsah odpovědi - tj response.data)

      ... zdrojový kód TODO 3 definujte routu pro OrderDetailController -->
                                                                                                                    $routeProvider
                                                                                                                        .when('/detail/:id', {
                                                                                                                            templateUrl: 'orderDetail.html',
                                                                                                                            controller: 'OrderDetailController',
                                                                                                                            controllerAs: 'detail',
                                                                                                                            resolve: {
                                                                                                                              orderData: function($http, $route, REST_URI) {
                                                                                                                                return $http.get(REST_URI + '/orders/' + $route.current.params.id)
                                                                                                                                    .then(function(response) {
                                                                                                                                      return response.data;
                                                                                                                                    });
                                                                                                                              }
                                                                                                                            }
                                                                                                                          })


    - všimněte si rozdílu chování
                                                                                                                    pokud si controller závislost řeší sám, uvidíte probliknutí dat
                                                                                                                    když použijete resolve, controller se zobrazí až když jsou data načtená - čekáte déle, ale nevidíte probliknutí

4. přidejte do detailu odkaz zpět na seznam

    ... zdrojový kód TODO 4 přidejte do detailu odkaz zpět na seznam -->
                                                                                                          <a class="back" ng-href="#/orders/">Zpět na seznam</a>
