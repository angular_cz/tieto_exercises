angular.module('orderAdministration', ['ngRoute'])
  .constant('REST_URI', 'http://orders-api.angular.cz')
  .config(function($routeProvider) {
    
    // TODO 1 definujte routu pro OrderListController
    // TODO 2 zajistěte přesměrování na seznam
    // TODO 3 definujte routu pro OrderDetailController

    })
    .controller('OrderListController', function($http, REST_URI) {
      this.orders = [];

      this.statuses = {
        new : 'Nová',
        made: 'Vyrobená',
        cancelled: 'Zrušená',
        paid: 'Zaplacená',
        sent: 'Odeslaná'
      };

      this.onOrdersLoad = function(orders) {
        this.orders = orders;
      };

      $http.get(REST_URI + '/orders')
          .then(function(response) {
            return response.data;
          })
          .then(this.onOrdersLoad.bind(this));

    })
    .controller('OrderDetailController', function(orderData) {
      this.order = orderData;
    });