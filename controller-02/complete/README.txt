Controller-02

1. V definici controlleru v app.js přiřaďte data do this, místo do $scope
   ... zdrojový kód TODO 1 - přiřaďte data a funkce místo $scope do this
                                                                                                          this.user = {
                                                                                                            ...
                                                                                                          };

                                                                                                          this.getAge = function () {
                                                                                                            ...
                                                                                                          };
2. Upravte příklad na použití stylem "controller as"
   - jako název controlleru v šabloně použijte "ctrl"
     ... zdrojový kód TODO 2 - upravte použití controlleru jako controller as
                                                                                                          <body ng-controller="userController as ctrl" class="container">
3. V šabloně už nejsou data ve scope, ale v objektu ctrl
   - upravte použití v ng-model
     ... zdrojový kód TODO 3.1 - upravte použití v ng-model
                                                                                                          <input id="userName" ng-model="ctrl.user.name" class="form-control"/>
                                                                                                          <input id="userSurname" ng-model="ctrl.user.surname" class="form-control"/>
                                                                                                          <input id="yearOfBirth" ng-model="ctrl.user.yearOfBirth" class="form-control" type="number"/>
   - upravte použití ve výrazech
     ... zdrojový kód TODO 3.2 - upravte použití ve výrazech
                                                                                                          <div>Jméno: {{ctrl.user.name}}</div>
                                                                                                          <div>Příjmení: {{ctrl.user.surname}}</div>
                                                                                                          <div>Věk: {{ctrl.getAge()}}</div>