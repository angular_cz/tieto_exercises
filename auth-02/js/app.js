angular.module('authApp', ['ngRoute', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', 'http://security-api.angular.cz')

  .config(function ($routeProvider) {

    $routeProvider

      .when('/', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })

      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;

            return Orders.get({'id': id}).$promise;
          }
        }
      })

      .when('/create', {
        templateUrl: 'orderCreate.html',
        controller: 'OrderCreateController',
        controllerAs: 'create',
        resolve: {
          orderData: function (Orders) {
            return new Orders();
          }

          // TODO 6 přidejte závislost authorized,
        }
      })


      .otherwise('/');
  })

  .config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($injector, REST_URI) {

      return {
        responseError: function (response) {
          return $injector.invoke(function (loginModal, $http, $rootScope, $q) {

            if (response.status === 401) {
              if (response.config.url === REST_URI + '/login') {

                // TODO 2.3 vyslání zprávy restApi:loginFailed
              } else {

                // TODO 2.1 zobrazte modální okno pomocí prepareLoginModal()
                // TODO 2.2 callback .then pro promise modálního okna
              }
            } // TODO 4 přidejte reakci na 403, otevřete prepareRejectModal()

            return $q.reject(response);
          });
        }
      };
    });
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function () {
      console.log('Logout')
      $location.path("/")
    });
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function () {
      console.log('Chyba - selhal routing - zobraz /');

      //TODO 7 přesměrujte na /
    });
  });


