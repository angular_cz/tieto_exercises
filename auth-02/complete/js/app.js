angular.module('authApp', ['ngRoute', 'ngResource', 'ui.bootstrap'])
  .constant('REST_URI', 'http://security-api.angular.cz')

  .config(function ($routeProvider) {

    $routeProvider

      .when('/', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })

      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (Orders, $route) {
            var id = $route.current.params.id;

            return Orders.get({'id': id}).$promise;
          }
        }
      })

      .when('/create', {
        templateUrl: 'orderCreate.html',
        controller: 'OrderCreateController',
        controllerAs: 'create',
        resolve: {
          orderData: function (Orders) {
            return new Orders();
          },
          isAutorized: function (authService, loginModal) {
            return authService.user || loginModal.prepareLoginModal();
          }
        }
      })


      .otherwise('/');
  })

  .config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($injector, REST_URI) {

      return {
        responseError: function (response) {
          return $injector.invoke(function (loginModal, $http, $rootScope, $q) {
            if (response.status === 401) {
              if (response.config.url === REST_URI + '/login') {
                $rootScope.$broadcast('restApi:loginFailed');
              } else {
                return loginModal.prepareLoginModal()
                  .then(function () {
                    return $http(response.config);
                  });
              }
            } else if (response.status === 403) {
              return loginModal.prepareRejectModal();
            }

            return $q.reject(response);
          });
        }
      };
    });
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('login:loggedOut', function () {
      console.log('Logout')
      $location.path("/")
    });
  })

  .run(function ($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function (event) {
      console.log('Chyba - selhal routing - zobraz /');
      $location.path('/');
    });
  });


