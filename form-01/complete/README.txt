Form-01

1. vyzkoušejte že formulář funguje
  - jak je navázaný na  this.user
  - jak se mění třídy formuláře a prvků
  - jak se do modelu propagují validní/nevalidní hodnoty

2. validace
  - přiřaďte formulář do scope (controlleru) 

    ... zdrojový kód TODO 2.1 přiřaďte formulář do scope -->
                                                                                                          <form name="detail.userForm">

  - zajistěte správné zobrazení hlášek pro
     - jméno uživatele (povinná položka), použijte ng-if

        ... zdrojový kód TODO 2.2 zajistěte správné zobrazení hlášek pro jméno uživatele -->
                                                                                                          <div class="text-danger" ng-if="detail.userForm.name.$invalid">
     - email (povinná položka, typ email) - použijte ng-messages

         ... zdrojový kód TODO 2.3 zajistěte správné zobrazení hlášek pro email -->
                                                                                                          <div ng-messages="detail.userForm.email.$error" class="text-danger">
                                                                                                            <div ng-message="required">You did not enter a email</div>
                                                                                                            <div ng-message="email">Your email is not valid</div>
                                                                                                          </div>
      
   - pro user-friendly chování zobrazujte hlášky když je element.$touched
         ... zdrojový kód TODO 2.4 zajistěte user-friendly chování -->
                                                                                                          <div ng-if="(detail.userForm.name.$touched || detail.userForm.$submitted) && detail.userForm.name.$invalid"
                                                                                                               class="text-danger">

                                                                                                          <div ng-messages="detail.userForm.email.$error"
                                                                                                               ng-show="detail.userForm.email.$touched || detail.userForm.$submitted"
                                                                                                               class="text-danger">


3. vlastní validátor na formát telefonního čísla
    - nastavte definiční objekt directivy validátoru, tak aby:
      -  byla omezená jako atribut, 
      - měla závislost na ngModel
      - ngModel.$validators přidala validátor czechPhoneNumber
      - pozor na to, že validátor musí projít, pokud je hodnota undefined, nebo prázdný string,
          na tyto hodnoty reaguje pouze validátor require

         ... zdrojový kód TODO 3.1 nastavte definiční objekt directivy validátoru -->

                                                                                                                  return {
                                                                                                                    restrict: 'A',
                                                                                                                    require: 'ngModel',
                                                                                                                    link: function(scope, element, attrs, ngModel) {
                                                                                                                      ngModel.$validators.czechPhoneNumber = function(value) {
                                                                                                                        if (ngModelController.$isEmpty(value)) {
                                                                                                                          return true;
                                                                                                                        }

                                                                                                                        return pattern.test(value);
                                                                                                                      };
                                                                                                                    }
                                                                                                                  };

    - použijte validátor a zobrazujte chybovou zprávu dle výsledku validace
      - použití validátor v inputu
      - podmíněné zobrazení chyby podle phone.$error
        (pozor na validate-czech-phone-number vs validateCzechPhoneNumber)
         ... zdrojový kód TODO 3.2 použijte validátor -->
                                                                                                                  <input class="form-control" type="text" name="phone" id="phone" validate-czech-phone-number ng-model="detail.user.phone" />
                                                                                                                  <div ng-if="(detail.userForm.phone.$touched || detail.userForm.$submitted) && detail.userForm.phone.$error.czechPhoneNumber" class="text-danger">
                                                                                                                    Use format +420 123 456 789
                                                                                                                  </div>


   - zjistěte jak se validátor projeví v css třídách prvku
                                                                                                                  objevuje se třída ng-(in)valid-czech-phone-number

4. propagace položky City až po opuštění pole
  - ng-model-options (https://docs.angularjs.org/api/ng/directive/ngModelOptions)

         ... zdrojový kód TODO 4 propagace položky City až po opuštění pole -->
                                                                                                                  <input class="form-control" type="text" id="city" ng-model="detail.user.address.city" ng-model-options="{ updateOn: 'blur'}" />

5. odeslání formuláře
    - odeslání formuláře provede detail.save()
         ... zdrojový kód TODO 5 odeslání formuláře -->
                                                                                                                  <form role="form" name="detail.userForm" ng-submit="detail.save()">

6. další tlačítka a akce

    - tlačítka vyvolájí metody:
      reset -> detail.reset()
      clear storage -> detail.clear()

         ... zdrojový kód TODO 6.1 další tlačítka a akce -->
                                                                                                                  <button type="button" class="btn btn-warning" ng-click="detail.reset()">Reset</button>
                                                                                                                  <button type="button" class="btn btn-danger" ng-click="detail.clear()">Clear Storage</button>

    - zajistěte, aby bylo tláčítko reset disabled, když nejsou ve formuláři neuložené změny

         ... zdrojový kód TODO 6.2 další tlačítka a akce -->
                                                                                                                  <button type="button" class="btn btn-warning" ng-disabled="detail.userForm.$pristine" ng-click="detail.reset()">Reset</button>

7. Extra

    - vyplňte formulář, uložte data a sledujte kam se propagují další změny hodnot
    - ve službě userStorage.get a userStorage.set odstraňte angular.copy, opět uložte data a sledujte kam se propagují další změny hodnot
      Je vám jásné proč je třeba vracet kopii dat?
