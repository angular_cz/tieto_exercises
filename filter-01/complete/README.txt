Filter-01

1. přidat filter uppercase tam kde má být text velkým

    ... zdrojový kód TODO 1 - přidat filter uppercase -->
                                                                                                                <li>Velkým:                 {{filterCtrlinputText|uppercase}}</li>

2. vytvořit vlastní filter reverse
  
  Pro vlastní filtr jsou připraveny testy. Spustíte je příkazem npm run filter-01-karma
  Doporučujeme si vyzkoušet TDD - spusťte testy a implementujte kód filtru. 
  Po uložení souboru se vám testy automaticky pustí znovu.
  Měli by jste být schopni napsat celý filter proti testům bez pohledu do prohlížeče.

   - vracená funkce by měla mít dva argumenty - input, prefix
   - pozor na undefined u prefixu - nastavte defaultní hodnotu perexu na prázdný řetězec
   - funkce pro otočení - input.split("").reverse().join("");

    ... zdrojový kód TODO 2 - vytvořit vlastní filter reverse -->
                                                                                                                .filter("reverse", function () {
                                                                                                                  return function (input, prefix) {
                                                                                                                    input = input || "";
                                                                                                                    prefix = prefix || "";

                                                                                                                    var reversed = input.split("").reverse().join("");
                                                                                                                    return prefix + reversed;
                                                                                                                  };
                                                                                                                });

3. použití vlastního filteru

 - použít filter reverse

    ... zdrojový kód TODO 3.1 - použít filter reverse -->
                                                                                                                <li>Pozpátku:               {{filterCtrlinputText|reverse}}</li>

 - použít filter reverse v kombinaci s uppercase

    ... zdrojový kód TODO 3.2 - použít filter reverse v kombinaci s uppercase -->

                                                                                                                <li>Pozpátku a velkým:      {{filterCtrlinputText|reverse|uppercase}}</li>

 - použít filter reverse spolu s nějakým hezkým prefixem, třeba "Le "

    ... zdrojový kód TODO 3.3 - použít filter revers prefixem -->

                                                                                                                <li>Pozpátku a s prefixem:  {{filterCtrlinputText|reverse:"Le "}}</li>

4. zformátujte datum ve výpisu ticketů
 - jako formát datumu použijte - den. měsíc. rok
 - dokumentace : https://docs.angularjs.org/api/ng/filter/date
    ... zdrojový kód TODO 4 - formátujte datum "d. M. yyyy" -->
                                                                                                                <td>{{todo.duedate| date:"d. M. yyyy"}}</td>

5. Omezte výpis ticketů
 - zobrazte jen otevřené tickety - status = 'open'
 - objekt search je inicializován v controlleru.
     ... zdrojový kód TODO 5.1a - filterujte tickety podle stavu -->
                                                                                                                this.search = {
                                                                                                                  status: 'open'
                                                                                                                };
 - v šabloně filterujte pomocí tohoto objektu
     ... zdrojový kód TODO 5.1b - filterujte tickety podle stavu -->
                                                                                                                | filter : filterCtrl.search


 - rozšiřte filterování podle jména zadaného v poli hledání - attribut owner
     ... zdrojový kód TODO 5.2 - filterujte tickety podle jména -->
                                                                                                                Hledání podle vlastníka: <input type="text" ng-model="filterCtrl.search.owner" class="form-control"/>

 - seřaďte tickety podle atributu duedate
     ... zdrojový kód TODO 5.3 - seřaďte tickety -->
                                                                                                                | orderBy: 'duedate'

 - omezte počet ticketů ve výpisu na 5
     ... zdrojový kód TODO 5.4 - omezte počet ticketů -->
                                                                                                                | limitTo:5">

