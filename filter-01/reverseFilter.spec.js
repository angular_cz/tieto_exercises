describe('reverseFilter', function() {

  beforeEach(module('filterApp'));

  it('should return empty string when input is empty', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter('')).toEqual('');
    });
  });

  it('should return reverded string', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter('123')).toEqual('321');
      expect(reverseFilter('dlouhý text s mezerami')).toEqual('imarezem s txet ýhuold');
    });
  });

  it('should return reverded string with prefix', function() {
    inject(function(reverseFilter) {
      expect(reverseFilter('', 'prefix')).toEqual('prefix');
      expect(reverseFilter('123', 'prefix')).toEqual('prefix321');
    });
  });

});