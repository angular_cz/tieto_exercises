Directive-02;

než začnete, prohlédněte si použití directivy v index.html (ř. 14)

1. TODO 1 - doplňte definiční objekt directivy baconIpsum
  - cílem tohoto cvičení je postupné budování definičního objektu direktivy v app.js

  - definujte šablonu directivy 'baconIpsum.html'

    ... zdrojový kód najdete vpravo -->
                                                                                                          templateUrl: 'baconIpsum.html'

  - definujte metodu link a v ní přidejte na scope vlastnost data,
  - jako hodnotu jí předejte fakeData

    ... zdrojový kód najdete vpravo -->
                                                                                                          scope.data = fakeData;
 
  - prohlédněte si šablonu baconIpsum.html a ověřte, že se directiva správně vykresluje

  - pro získání dat (scope.data) použijte službu generator, formát dat je stejný jako v proměnné fakeData
    // metoda má signaturu generator.getParagraphs(count = 1)

    ... zdrojový kód najdete vpravo -->
                                                                                                          scope.data = generator.getParagraphs();

2. TODO 2 - directiva s parametrem
  - odkomentujte TODO 2 v index.html a všimněte si parametru paragraphs=2

  - chceme, aby počet odstavců bylo možné nastavit tímto parametrem, proto
    předejte hodnotu attributu jako první parametr metody generator.getParagraphs

    ... zdrojový kód najdete vpravo -->
                                                                                                          link:function(scope, element, attributes) {
                                                                                                            scope.data = generator.getParagraphs(attributes.paragraphs);
                                                                                                          }

  - podívejte se jak se directiva chová a vyřešte problém s "přepisováním dat" 
    (nesmíme sdílet scope, zděděný scope nám bude stačit)

    ... zdrojový kód najdete vpravo -->
                                                                                                          scope:true

3. TODO 3 - dynamický změna titulku
  - odkomentujte TODO 3 v index.html a všimněte si parametru title="{{bacon.title3}}"
    chceme, aby directiva reflektovala parametr title jako titulek

  - upravte directivu tak, aby používala izolovaný scope 
    a přijímala parametr title jako expression

    ... zdrojový kód najdete vpravo -->
                                                                                                          scope:{
                                                                                                            title: '@'
                                                                                                          },

  - title už je v šabloně připraven, nyní zkontrolujte, že binding funguje
    (titulek se mění při změně inputu)

  - zamyslete se, kde všude můžete použít one time binding, pro snižení počtu watcherů

4. EXTRA

 - pokud jste došli až sem, můžete si vyzkoušet ještě dvoucestný databinding

 - v definici izolovaného scope přidejte vlastnost title2way, mapovanou jako dvoucestný databinding
 - do šablony directivy vložte input s ngModel="title2way"
 - upravte použití directivy tak, aby se do title2way předávala vlastnost bacon.title3

 - je jasné
    - jak se liši použití = a @ ?
    - jaké vstupy může který parametr přijímat?
    - jak zapsat parametry při použití directivy?

5. EXTRA EXTRA
 - pokud ještě nemáte dost, vyzkoušejte dynamickou změnu počtu paragrafů
 - do šablony vložte input type="number"  ngModel="bacon.paragraphs3"
 - předejte model direktivě jako expression ({{...}}
 - uvnitř direktivy sledujte změny atributu paragraphs pomocí $observe
 - při změně aktualizujte data.
 - vyzkoušejte použití deregistračního callbacku v události $destroy

 - je jasné
    - proč se bez sledování pomocí $observe změna atributu neprojeví?
    - proč je nutné použít expression?
    - jak deregistrovat sledované zdálosti?
