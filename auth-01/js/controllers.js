angular.module('authApp')

  .controller('HomeCtrl', function ($scope, $location, authService) {

    this.user = authService.user;

    $scope.$on('login:loginFailed', function () {
      this.isBadLogin = true;
    }.bind(this));

    $scope.$on('login:loginSuccess', function () {
      $location.path("/orders");

      this.isBadLogin = false;
    }.bind(this));

    this.loginUser = function () {
      authService.login("user", "password");
    };

    this.loginAdmin = function () {
      authService.login("operator", "password");
    };

    this.loginFail = function () {
      authService.login("badUser", "badPassword");
    };
  })

  .controller('OrderListController', function (Orders, authService) {

    //TODO 7.1 - uživatel musí mít roli ROLE_USER

    var orderCtrl = this;
    this.auth = authService;
    this.orders = Orders.query();

    this.statuses = {
      NEW: 'Nová',
      CANCELLED: 'Zrušená',
      PAID: 'Zaplacená',
      SENT: 'Odeslaná'
    };

    this.removeOrder = function (order) {
      order.$remove(function () {
        var index = orderCtrl.orders.indexOf(order);
        orderCtrl.orders.splice(index, 1);
      });
    };

    this.updateOrder = function (order) {
      order.$save();
    };
  })
  .controller('OrderDetailController', function (orderData, authService) {
    //TODO 7.1 - uživatel musí mít roli ROLE_USER

    this.order = orderData;
  })
  .controller('OrderCreateController', function (orderData, $location, authService) {
    //TODO 7.2 - uživatel musí mít roli ROLE_OPERATOR

    this.order = orderData;

    this.save = function () {
      this.order.$save(function () {
        $location.path("/orders");
      });
    };
  });