Auth-01

Aplikaci zobrazení objednávek rozšíříme o autentizaci.
Rozhraní je stejné jako v příkladu objednávek, ale jsou zde přidané požadavky na oprávnění.

Nepřihlášený uživatel nemá přístup k ničemu,
Jsou zde dvě role ROLE_USER, ROLE_OPERATOR

ROLE_USER může zobrazit zdroje pomocí metody GET - /orders a /orders/:id
ROLE_OPERATOR může vytvářet nový záznam, mazat a měnit stav stávajících

Hlavním úkolem je oživit přihlášení a obohatit aplikaci o prvky rozdělující chování pro role

Pokud budete mazat záznamy, mažte pokud možno, ty, které jste sami vytvořili.

Postup:
-------

1. Vyzkoušejte přejít na záložku s objednávkami, v consoli je vidět odpověď 401

  - na úvodní straně jsou tlačítka simulující přihlašovací formulář na úvodní straně
  - podívejte se co dělají (HomeCtrl - controllers.js)

2. Oživení přihlášení
  - v authService (services.js) v metodě login, odešlete postem přihlašovací údaje
    na adresu REST_URI + '/login'
  - jako reakci použijte funkce successLogin a failedLogin
    ... zdrojový kód TODO 2.1 - odeslat přihlášení -->
                                                                                                          return $http.post(REST_URI + "/login", credentials)
                                                                                                                    .then(successLogin, failedLogin);

  - ve funkci successLogin naplňte získaným tokenem (response.data.token),
    hlavičku 'X-Auth-Token' v nastavení $http.defaults.headers.common
    ... zdrojový kód TODO 2.2 - přidat token do defaultní hlavičky -->
                                                                                                          $http.defaults.headers.common['X-Auth-Token'] = response.data.token;

  - odešlete pomocí $broadcast zprávu "login:loginSuccess"
    ... zdrojový kód TODO 2.3 - odeslat zprávu login:loginSuccess -->
                                                                                                          $rootScope.$broadcast("login:loginSuccess");

  - podívejte se, kdo ji přijímá a jak (controllers.js:HomeCtrl)

3. Oživení odhlášení
  - v direktivě authLogoutLink zavolejte metodu authService (directives.js)
  - podívejte se co metoda dělá
    ... zdrojový kód TODO 3.1 - odhlášení -->
                                                                                                          authService.logout()

  - v sekci .run (app.js) reagujte na příjem události "login:loggedOut" přesměrováním na /
    ... zdrojový kód TODO 3.2 - reakce na zprávu login:loggedOut -->
                                                                                                          $rootScope.$on('login:loggedOut', function () {
                                                                                                            $location.path("/")
                                                                                                          });
  - direktivu přidejte na Logout tlačítko (index.html)
    ... zdrojový kód TODO 3.3 - použijte direktivu pro odhlášení -->
                                                                                                          <button auth-logout-link ...

  - všimněte si při tom taky dalších direktiv auth-is-authenticated, auth-user-name

4. Vyzkoušejte
  - přihlašte se napřed jako operátor,
  - vyzkoušejte přechod na výpis objednávek
  - vytvoření a smazání záznamu

  - odhlášení

  - přihlašte se jako uživatel
  - požadavek na smazání záznamu hlásí 403 v konzoli

5. Kontrola rolí při zobrazení prvků na výpisu
  - orderList.html
    - select se stavy zobrazte pouze pro roli ROLE_OPERATOR
    - stejně tak tlačítko pro mazání
    - můžete využít service authService, kterou má controller předanou do scope a metodu hasRole
      ... zdrojový kód TODO 5.1 zobrazit pouze pro roli ROLE_OPERATOR -->
                                                                                                          <select ng-if="list.auth.hasRole('ROLE_OPERATOR')"

                                                                                                          <a ng-if="list.auth.hasRole('ROLE_OPERATOR')" ng-click="list.removeOrder(order)" class="btn btn-danger">Odstranit</a>


    - pro uživatele, kteří nemají roli ROLE_OPERATOR zobrazte pouze text
      ... zdrojový kód TODO 5.2 zobrazit text pouze pro uživatele, který nemá ROLE_OPERATOR -->
                                                                                                          <span ng-if="!list.auth.hasRole('ROLE_OPERATOR')">{{list.statuses[order.status]}}</span>

 - podívejte se na direktivu auth-has-role a použijte ji v menu (index.html)
   - pro odkaz na objednávky /orders role - ROLE_USER
   ... zdrojový kód TODO 5.3 - zobrazení objednávek pro roli ROLE_USER -->
                                                                                                          <ul auth-has-role="ROLE_USER" class="nav navbar-nav">

   - pro odkaz na přidání - ROLE_OPERATOR
   ... zdrojový kód TODO 5.4 - zobrazení tlačítka pro roli ROLE_OPERATOR -->
                                                                                                          <span auth-has-role="ROLE_OPERATOR" class="navbar-form navbar-left">

6. Vyzkoušejte, že se zobrazují pouze věci, dostupné pro roli uživatele.

7. Zkuste na detailu nebo na vytváření obnovit stránku, stránka se sice zobrazí,
    ale v consoli se objeví 401, protože informace o přihlášení se ztratí

  - pro vynucení konkrétní role při startu controlleru použijte authService.mustHaveRole()
  - controllers.js
    - v controllerech pro list a detail musí uživatel roli ROLE_USER
    - v create controlleru - roli ROLE_OPERATOR

     ... zdrojový kód kontroly TODO 7.x - uživatel musí mít roli x -->
                                                                                                         authService.mustHaveRole("ROLE_OPERATOR");

                                                                                                         authService.mustHaveRole("ROLE_USER");

  - vyzkoušejte znovu obnovení a přístup na vytvoření nového prvku pokud je přihlášen jen uživatel