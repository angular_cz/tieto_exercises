DI-02

Nyní si vyzkoušíte přepis factory calculator na provider a konfiguraci cen

1. ověřte, že projdou testy
   - spusťe v terminálu

   - npm run di-02-karma

   - výsledkem by měly být dva spuštené testy.
   - karma runner nyní sleduje změny a spouští testy automaticky

2. Přepište factory calculator na provider a zkontrolujte pomocí testů
  - stávající obsah factory bude vracen metodou $get
  - injektáž závislosti logger má být na správném místě
    ... zdrojový kód - TODO 2 - přepište factory calculator na provider -->
                                                                                                          .provider('calculator', function () {

                                                                                                            this.$get = function (logger) {
                                                                                                              return {
                                                                                                                getPrice: function (product) {
                                                                                                           ...
3. Rozšíření provideru o možnost konfigurace.
  - přesunout definici proměnné pagePrice do provideru
    nad definici getPrice(product)
  - přidejt setter -  setPagePrice(price)

    ... zdrojový kód - TODO 3 - přesun do provideru a přidání setterů -->
                                                                                                          .provider('calculator', function () {
                                                                                                              var baseCoverPrice = 70;
                                                                                                              var pagePrice = 3;

                                                                                                              this.setPagePrice = function(price) {
                                                                                                                pagePrice = price;
                                                                                                              };

                                                                                                              this.$get = function (logger) {
4. Konfigurace provideru v konfigurační fázi
  - přidejte volání metody .config() a volanou funkci
  - injektněte calculatorProvider
  - nastavte page price na jinou cenu setPagePrice(4)
  - v tuto chvíli testy nebudou fungovat

    ... zdrojový kód - TODO 4 - konfigurace provideru -->
                                                                                                           .config(function(calculatorProvider){
                                                                                                              calculatorProvider.setPagePrice(4);
                                                                                                            })
5. Opravte testy
  - v testu je nutno v beforeEach zavolat .config modulu diApp
  - v konfiguraci pak nastavit page price pro účely testu na hodnotu 3

    ... zdrojový kód - TODO 5 - konfigurace provideru pro učely testu -->
                                                                                                           angular.module('diApp')
                                                                                                                .config(function(calculatorProvider) {
                                                                                                                  calculatorProvider.setPagePrice(3);
                                                                                                                });
6. Použití konstanty
  - vytvořte konstantu PAGE_PRICE, obsahující cenu - 4
  - injektněte konstantu konstantu do .config a použíjte ji v setteru
    ... zdrojový kód - TODO 6 - použití kontanty -->
                                                                                                           .constant('PAGE_PRICE', 4)

                                                                                                          .config(function(calculatorProvider, PAGE_PRICE){
                                                                                                            calculatorProvider.setPagePrice(PAGE_PRICE);
                                                                                                          })
