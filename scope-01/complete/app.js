angular.module('moduleExample', [])
    .controller('UserController', function () {
      this.user = {
        name: 'Petr',
        surname: 'Novák',
        yearOfBirth: 1982
      };

      this.getAge = function () {
        console.log('getAge');
        var now = new Date();
        return now.getFullYear() - this.user.yearOfBirth;
      };
    });
