Scope-01

Vyzkoušíte si, jak Angular vyhodnocuje výrazy.

1. Vyzkoušejte, funkčnost databindingu a aktualizace všech atributů
   - Podívejte se na definici controlleru v app.js a použití v index.html

2. V metodě getAge v kontrolleru odkomentujte console.log
   - sledujte, kolikrát je metoda volána při změně jednotlivých atributů.

   - vyzkoušejte změnu jména a poté změnu věku
   - proč je tomu tak? Vzpomeňte si na povídání o principu dirty checkingu

3. Použijte v index.html pro výpis jména a věku one-time binding, sledujte změnu chování
   ... řešení TODO 3 - doplňte one-time binding  -->
                                                                                                           Jméno: {{::ctrl.user.name}}
                                                                                                           Věk: {{ctrl.getAge()}}