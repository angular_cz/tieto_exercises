# Cvičení - www.angular.cz #

## Co si nainstalovat
- git - http://git-scm.com/downloads
- aktuální verzi nodeJS - http://nodejs.org/download/

## NPM závislosti
Nodejs se nainstaloval spolu s balíčkovacím nástrojem npm. Ten použijeme pro instalaci dalšího nástroje:

 - bower - nástroj na správu javascriptových závislostí
 
###Instalaci provedete spuštěním
```
npm install -g bower
``` 

## Ověření správnosti instalace

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/tieto_exercises
```

Spusťte v jeho adresáři následující příkazy, žádný z nich by neměl vyhlásit chybu (první může chvíli trvat).

```
 npm install

 npm start
```

Nyní, když otevřete prohlížeč na adrese http://localhost:8000/ uvidíte seznam cvičení.

Prima, první půlku máme za sebou. 


### Test protractoru ###

Nyní nechte spuštěný příkaz "npm start" a v novém terminálu ve složce projektu spusťte příkaz.

```
 npm run test-protractor
```

Příkaz by opět neměl hlásit chybu . Měli by jste vidět výstup testu:

```
Starting selenium standalone server...
Selenium standalone server started at http://192.168.1.17:56131/wd/hub
.

Finished in 1.515 seconds
1 test, 1 assertion, 0 failures
```

Pokud vše proběhlo podle postupu, máte správně připravené prostředí. 
V opačném případě se podívejte do sekce možné problémy a pokud tam nenaleznete řešení, kontaktujte nás.

## Použití při cvičeních: ##

Všechny potřebné nástroje a závislosti jste nainstalovali už v předchozích krocích pomocí příkazu "npm install".

### Při dalším spouštění už si tak vystačíte s příkazem: ###
```
npm start
```
který spustí lokální server na adrese http://localhost:8000/

### Spuštění testů v jednotlivých cvičeních ###
V některých cvičeních jsou navíc přítomny testy, které je možné spustit pomocí následujících příkazů. Příkazy se spouští v kořenovém adresáři balíčku.

```
# controller-03
npm run controller-03-protractor

# filter-01
npm run filter-01-karma

# di-01
npm run di-01-karma

# di-01
npm run di-01-unitTesting-karma

# di-02
npm run di-02-karma

# router-01
npm run router-01-protractor
```

### Možné probémy ###

####unable to connect to github.com####
pokud vidíte tuto chybovou zprávu po spuštění *bower install*
 - máte buď blokováno připojení ke githubu  - to můžete ověřit otevřením github.com v prohlížeči
 - nebo máte blokován protokol git - spusťte příkaz, který "přesměruje" protokol git po https

```
git config --global url."https://github.com/".insteadOf git@github.com:
git config --global url."https://".insteadOf git://
```

Nyní už by měl příkaz *bower install* fungovat

Pokud problémy přetrvávají, a jste uživatelem systému Windows, může zde být následující problém.

 - Nastavení výše se zapíšou do .gitconfig do domovské složky, na Windows s profilem na vzdáleném disku však může každý ze shelů hledat home jinde.
 - Pokud je toto Váš případ, zkopírujte soubor do obou umístění, na sdílenou domovskou složku a c:\users\[name]\

Další variantou je konfigurovat každý projekt samostatně (vynechat atribut --global)


```
git config url."https://github.com/".insteadOf git@github.com:
git config url."https://".insteadOf git://
```

####spawn child ENOENT error####
pokud při spuštění *npm run update-webdriver*, nebo *npm run protractor* vidíte tuto chybu, pravděpodobně nemáte nainstalovanou JAVU, která je nutná pro jeho spuštění. Pozor na to, že na windows musíte po instalaci javy spustit příkazovou řádku znovu, jinak se bude chyba opakovat.

####Error: Angular could not be found on the page http://localhost:8888/app/index.html : retries looking for angular exceeded####
pokud při  *npm run protractor* vidíte tuto chybu, ověřte, že na adrese http://localhost:8888/app/index.html je vidět testovací stránka. Pokud není, pravděpodobně jste vypnuly konzoli se spuštěným "npm start" po první části testu

####Adresa pro ruční stažení chrome driveru####
- Chromedriver: http://chromedriver.storage.googleapis.com/index.html?path=2.10/
- Selenium server : https://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar

#### Codio repozitář ####
https://codio.com/Angular/tieto-exercises-02/