Directive-01

Vyzkoušejte si standardní direktivy Angularu pro ovlivňování view

1. Podle výběru typu nemovitosti zobrazte div s odpovídajícími parametry
  - kontrolujte reality.type - ground/house
  - u pozemku zobrazte jen parametry pozemku
  - u domku zobrazte jak parametry pozemku, tak parametry domu
  - komerční pozemek nemá žádný typ
  - jednou použijte ng-if a podruhé ng-show
    ... zdrojový kód - TODO 1 - použijte ng-if nebo ng-show -->
                                                                                                          <div ng-show="reality.type == 'house'">
                                                                                                            ...

                                                                                                          <div ng-if="reality.type == 'ground'">
                                                                                                            ...
  - podívejte se na rozdíl mezi ng-if a ng-show v devtools

2. Oživení seznamu lokalit
  - použijte ng-repeat na divu, který chcete opakovat
  - vykreslete jej z pole reality.localities
  - jako value použijte hodnotu location.key
    ... zdrojový kód - TODO 2 - oživení seznamu lokalit -->
                                                                                                          <div ng-repeat="locality in reality.localities" class="radio col-md-6">
                                                                                                            <label>
                                                                                                              <input type="radio" ng-model="reality.selectedLocality" name="locality"  value="{{locality.key}}" />
                                                                                                              {{locality.name}}
                                                                                                            </label>
                                                                                                          </div>
3. Vložte copyright.html pomocí ng-include nakonec stránky
  - použijte ng-include jako atribut
  - pozor na  apostrofy v expression - '...'
    ... zdrojový kód - TODO 3 - vložení copyrightu -->
                                                                                                            <div class="small text-center" ng-include="'copyright.html'"></div>

4. Vytvoření záložek v sekci informace
  - podívejte se na metody controlleru InfoCtrl v app.js
  - doplňte  tlačítka o událost na kliknutí ng-click,
  - využijte volání již definované funkce info.setActiveTab(<nazev>)
  - jako název použijte "about", "reference", "contacts"
    ... zdrojový kód - TODO 4.1 - akce pro výběr tabu -->
                                                                                                          <button type="button" ng-click="info.setActiveTab('about')" ...
                                                                                                          <button type="button" ng-click="info.setActiveTab('reference')" ...
                                                                                                          <button type="button" ng-click="info.setActiveTab('contacts')" ...
  - použijte ng-switch na div nadřazený budoucím záložkám, využijte funkci info.getActiveTab()
  - ng-switch-when pro jednotlivé divy záložek by měl odpovídat názvům použitým výše.
    ... zdrojový kód - TODO 4.2 - zobrazení tabu -->
                                                                                                          <div ng-switch="info.getActiveTab()">
                                                                                                            <div ng-switch-when="about">
                                                                                                              ...
                                                                                                            </div>
                                                                                                            <div ng-switch-when="reference">
                                                                                                              ...
                                                                                                            </div>
                                                                                                            <div ng-switch-when="contacts">
                                                                                                              ...
                                                                                                            </div>
                                                                                                          </div>
5. Zvýraznění aktuálně vybraného tabu
  - použijte ng-class ve formátu {},
  - jako třídu na levé straně použijte 'active'
  - jako podmínku pak funkci info.isTabActive(<nazev>)
    ... zdrojový kód - TODO 5 - zvýraznění tabu -->
                                                                                                          <button type="button" ng-class="{active : info.isTabActive('about')}" ...
                                                                                                          <button type="button" ng-class="{active : info.isTabActive('reference')}" ...
                                                                                                          <button type="button" ng-class="{active : info.isTabActive('contacts')}" ...
--- EXTRA ---

6. Přidání šablony do template cache
  - pokud vnímáte bliknutí copyrightu přidaného pomocí ng-include
    je to tím, že se provádí ajax požadavek pro stažení šablony
  - podívejte se v consoli, že je copyright.html načítán asynchronně
  - použijte $templateCache v sekci .run
  - přidejte šablonu s klíčem copyright.html a obsahem souboru
  - původní soubor můžete smazat

