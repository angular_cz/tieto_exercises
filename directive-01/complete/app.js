'use strict';

angular.module('directiveApp', [])
  .controller('RealityCtrl', function () {
    this.localities = [
      {name: 'Jihočeský', key: 1},
      {name: 'Jihomoravský', key: 2},
      {name: 'Karlovarský', key: 3},
      {name: 'Královéhradecký', key: 4},
      {name: 'Liberecký', key: 5},
      {name: 'Moravskoslezský', key: 6},
      {name: 'Olomoucký', key: 7},
      {name: 'Pardubický', key: 8},
      {name: 'Plzeňský', key: 9},
      {name: 'Praha', key: 10},
      {name: 'Středočeský', key: 11},
      {name: 'Ústecký', key: 12},
      {name: 'Vysočina', key: 13},
      {name: 'Zlínský', key: 14}
    ];

    this.selectedLocality = 13;
  })
  .controller('InfoCtrl', function () {
    var currentTab = 'about';

    this.setActiveTab = function (tab) {
      currentTab = tab;
    };

    this.isTabActive = function (tab) {
      return currentTab === tab;
    };

    this.getActiveTab = function () {
      return currentTab;
    };
  }).run(function ($templateCache) {
    $templateCache.put("copyright.html", "@ Angular.cz 2014-2015");
  });