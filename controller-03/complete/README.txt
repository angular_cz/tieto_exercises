Controller-03

Vyzkoušejte si end2end testy v nástroji protractor

1. spusťtě e2e test
   - musíte mít stále spuštěný server s příklady
   - v novém terminálu v rootu příkladů spusťte

   npm run controller-03-protractor

2. podívejte se jak je test napsaný
   - protractor-test.js

3. zkuste dopsat test, který testuje, že změna jména na Josef se projeví ve výpisu
   - pojmenujte jej "should change name if model is changed"
   - vyjít můžete z testu "should change age if model is changed" a modifikujte:
     - .sendKeys
     - by.binding

   ... zdrojový kód TODO 3 - vytvořte test should change name if model is changed -->
                                                                                                          it('should change name if model is changed', function() {
                                                                                                              element(by.model('ctrl.user.name'))

                                                                                                                  .clear()
                                                                                                                  .sendKeys('Josef')
                                                                                                                  .then(function() {
                                                                                                                    expect(
                                                                                                                        element(
                                                                                                                            by.binding('ctrl.user.name')
                                                                                                                            ).getText()
                                                                                                                        ).toMatch('Josef');
                                                                                                                  });
                                                                                                            });