DI - 01 - unitTesting

1. Ověřte, že stále fungují testy

   npm run di-01-unitTesting-karma

   - výsledkem by měl být jeden úspěšný a jeden přeskočený test.
   - karma runner nyní sleduje změny a spouští testy automaticky

2. Prohlédněte si implementaci testu CalculatorCtrl.spec.js

3. Implementujte test case "should return 120 when format is A5 and number of pages is 50"
  - odstraňte volání pending (které Jasmine říká, že má test přeskočit)
  - implementujte test podle textového popisu
    ... zdrojový kód TODO 3 - implementujte test -->
                                                                                                           it('should return 120 when format is A5 and number of pages is 50', function() {
                                                                                                              controller.product = {
                                                                                                                pageSize: 'A5',
                                                                                                                numberOfPages: 50
                                                                                                              };

                                                                                                              expect(controller.getPrice()).toEqual(120);
                                                                                                            });
  - zkontrolujte, že testy prochází, nyní zde budou dva ve stavu SUCCESS

4. Napište test, který bude testovat přímo factory calculator (Calculator.spec.js)
 - použijte stejné test cases jako u CalculatorCtrl.spec.js
 - calculator injektujte pomocí konstrukce - inject(function(calculator) {
 - nezapomeňte inicializovat modul diApp
   ... zdrojový kód // TODO 4 - otestujte factory calculator -->
                                                                                                          describe('calculator', function() {

                                                                                                            beforeEach(module('diApp'));

                                                                                                            it('should return 90 when format is A5 and number of pages is 0', function() {
                                                                                                              inject(function(calculator) {

                                                                                                              var product = {
                                                                                                                pageSize: 'A5',
                                                                                                                numberOfPages: 0
                                                                                                              };

                                                                                                              expect(calculator.getPrice(product)).toEqual(90);
                                                                                                              });
                                                                                                            });

                                                                                                            ... druhý test vypadá obdobně
 - zkontrolujte, že testy prochází
 - původní testy z CalculatorCtrl.js odstraňte, nyní je testována přímo factory calculator

 - vytvořte mock služby logger - object literal metodou log
 - pomocí spyOn sledujte volání metody log
 - nahraďte skutečný logger pomocí volání - module({logger: loggerMock})
  ... zdrojový kód TODO 4.1 - vytvořte mock služby logger -->
                                                                                                            var loggerMock;

                                                                                                            beforeEach(function() {
                                                                                                              loggerMock = {
                                                                                                                log: function(message) {
                                                                                                                }
                                                                                                              };

                                                                                                              spyOn(loggerMock, 'log');

                                                                                                              module({logger: loggerMock});
                                                                                                            });

 - vytvořte test, který ověří zda byla služba logger zavolána
 - zavolejte calculator.getPrice s prázdným objektem
 - zkontrolujte zada byla volána metoda loggerMock.log
  ... zdrojový kód TODO 4.2 - test, který ověří zda byla služba logger zavolána -->
                                                                                                            it('should call logger', function() {
                                                                                                              inject(function(calculator) {
                                                                                                                calculator.getPrice({});
                                                                                                                expect(loggerMock.log).toHaveBeenCalled();
                                                                                                              });
                                                                                                            });

5. Přepiště CalculatorCtrl.spec.js tak, aby neprovolával factory calculátor, ale používal mock
  - vytvořte mock calculatorMock - object literal s metodou getPrice(price)
  - pomocí spyOn definujte návratovou hodnotu getPrice třeba - 42
  - předejte ji do CalculatorCtrl při získávání pomocí $controller jako calculator
    ... zdrojový kód TODO 5.1 vyytvořte mock factory calculator -->
                                                                                                            calculatorMock = {
                                                                                                              getPrice: function(product) {
                                                                                                              }
                                                                                                            };

                                                                                                            spyOn(calculatorMock, 'getPrice').and.returnValue(42);

                                                                                                            controller = $controller('CalculatorCtrl', {"calculator": calculatorMock});
  - vytvořte test, který zkontroluje, že controller vrací hodnotu, kterou jste předali spyOn
    ... zdrojový kód TODO 5.2 - test, který zkontroluje návratovou hodnotu -->
                                                                                                            it('should return value from calculator factory', function() {
                                                                                                              expect(controller.getPrice()).toEqual(42);
                                                                                                            });

  - vytvořte test, který ověří zda byla zavolána funkce getPrice a s jakým parametrem
    ... zdrojový kód TODO 5.3 - test, který zkontroluje, že je mock volán -->
                                                                                                            it('should call getPrice on factory with default product if getPrice is called', function() {
                                                                                                              controller.getPrice();
                                                                                                              expect(calculatorMock.getPrice).toHaveBeenCalledWith(controller.product);
                                                                                                              expect(calculatorMock.getPrice.calls.count()).toEqual(1);
                                                                                                            });
