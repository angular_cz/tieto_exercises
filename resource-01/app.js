//TODO 1 - přidejte závislost ngResource
angular.module('orderAdministration', ['ngRoute'])
  .constant('REST_URI', 'http://orders-api.angular.cz')
  .config(function ($routeProvider) {

    $routeProvider
      .when('/orders', {
        templateUrl: 'orderList.html',
        controller: 'OrderListController',
        controllerAs: 'list'
      })
      .when('/detail/:id', {
        templateUrl: 'orderDetail.html',
        controller: 'OrderDetailController',
        controllerAs: 'detail',
        resolve: {
          orderData: function (orders, $route) {
            var id = $route.current.params.id;

            //TODO 4 - načtení dat pro detail
            return null;
          }
        }
      })
      .when('/create', {
        templateUrl: 'orderCreate.html',
        controller: 'OrderCreateController',
        controllerAs: 'create'
      })
      .otherwise('/orders');
  })
  .factory('orders', function (REST_URI, $resource) {
    //TODO 2 - inicializujte $resource
    return null;
  })
  .controller('OrderListController', function (orders, $location) {
    var orderCtrl = this;

    //TODO 3 - načtení dat

    this.statuses = {
      NEW : 'Nová',
      CANCELLED: 'Zrušená',
      PAID: 'Zaplacená',
      SENT: 'Odeslaná'
    };

    this.removeOrder = function (order) {
      // TODO 7 - odstranění záznamu
      var index = orderCtrl.orders.indexOf(order);
      orderCtrl.orders.splice(index, 1);
    };

    this.updateOrder = function (order) {
      // TODO 6 - uložení záznamu
    };

  })
  .controller('OrderDetailController', function (orderData) {
    this.order = orderData;
  })
  .controller('OrderCreateController', function ($location, orders) {
    //TODO 5.1 - vytvoření nového objektu
    this.order = null;

    this.save = function(order) {

      //TODO 5.2 - uložení nového objektu
      $location.path("/detail/" + order.id);
    }
  });